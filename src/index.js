console.log('通用部分代码');
/**
 * commonJS的用时加载引入方式，可以切换成形式的用时加载,但commonJS方式更科学
 * 两个前置依赖会自动组成一个chunk
 */
require.ensure(['../dep/common1', '../dep/common2'], function (require) {
    if (!appName) {
        // 注意！这里是动态引入(在判断条件中，或者包含变量)，通过bundle-loader拆解chunk
        require('bundle-loader!../dep/' + deviceType);
    }
    else {
        require('bundle-loader!../dep/' + deviceType + appName);
    }
});

// 预加载common1和common2，但是不执行
// require.ensure(['../dep/common1', '../dep/common2'], function (require) {
//     // 尝试执行，实现预加载
//     // require('../dep/common1');
// });
